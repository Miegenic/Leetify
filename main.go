package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	var text string
	text = os.Args[1]

	dat, err := ioutil.ReadFile(text)
	check(err)

	fileNameData := strings.Split(text, ".")
	fmt.Println("FILENAME: ",fileNameData[0])
	fmt.Println("TYPE: ",fileNameData[1])

	if fileNameData[1] == "txt" {
		var transferedText string
		transferedText = Leetify(string(dat))
		fmt.Println("==== ORIGINAL TEXT ====")
		fmt.Println(string(dat))
		fmt.Println("==== TRANSFERED TEXT ====")
		fmt.Println(transferedText)

		error := ioutil.WriteFile(text + ".leet", []byte(transferedText), 0664)
		check(error)
	} else if fileNameData[1] == "html" {
		var transferedText string
		transferedText = LeetifyHtml(string(dat))
		fmt.Println("==== ORIGINAL TEXT ====")
		fmt.Println(string(dat))
		fmt.Println("==== TRANSFERED TEXT ====")
		fmt.Println(transferedText)

		error := ioutil.WriteFile(text + ".leet", []byte(transferedText), 0664)
		check(error)
	} else {
		var transferedText string
		transferedText = Leetify(string(dat))
		fmt.Println("==== ORIGINAL TEXT ====")
		fmt.Println(string(dat))
		fmt.Println("==== TRANSFERED TEXT ====")
		fmt.Println(transferedText)

		error := ioutil.WriteFile(text + ".leet", []byte(transferedText), 0664)
		check(error)
	}
}

func checkText(input string) bool {
	if input == "" {
		return false
	}
	return true
}

func changeChar(input string) string {
	modifiedInput := strings.ToLower(input)

	rand := rand.Intn(1000)
	if rand > 500 {
		return input
	} else {

		switch modifiedInput {
		case "i":
			return "1"
		case "e":
			return "3"
		case "a":
			return "4"
		case "s":
			return "5"
		case "o":
			return "0"
		default:
			return input

		}
	}
}

func Leetify(text string) string {
	var transfered string
	for _, c := range text {
		transfered += changeChar(string(c))
	}
	return transfered
}

func LeetifyHtml(text string) string {
	var transfered string
	var insideTag bool = true
	for _, c := range text {
		if string(c) == "<" {
			insideTag = true
		} else if string(c) == ">" {
			insideTag = false
		}

		if insideTag {
			transfered += string(c)
		} else if !insideTag {
			transfered += changeChar(string(c))
		}
	}
	return transfered
}
